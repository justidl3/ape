#include "ApeConfig.h"
#if defined __APPLE__
#include <mach-o/dyld.h>
#include <sys/syslog.h>
#endif


#if defined( DEBUG )
void Ape::Print( const char _FunctionName[], const char _FileName[], const unsigned int _LineNumber, const char* Sentence, ... )
{
	va_list argptr;

	APE_CONFIG_PRINT( "%s: File:%s Line:%u ", _FunctionName, _FileName, _LineNumber );

	va_start( argptr, Sentence );
	APE_CONFIG_VA_PRINT( Sentence, argptr );
	va_end( argptr );
	APE_CONFIG_PRINT( "\n" );
	return;
}
#endif //ifndef DEBUG
