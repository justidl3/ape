#ifndef __APE_CONFIG_H__
#define __APE_CONFIG_H__

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#define APE_CONFIG_PRINT printf
#define APE_CONFIG_VA_PRINT vprintf

#if defined( DEBUG )
#if defined( __cplusplus )
#define ape_print( ... ) Ape::Print( __func__, __FILE__, __LINE__, __VA_ARGS__ );

namespace Ape
{
	void Print( const char FunctionName[], \
	            const char FileName[], \
	            const unsigned int LineNumber, \
	            const char* Sentence, ... );
}

#else //if defined( __cplusplus )
#pragma message( "Using c variant of ape." )
static inline void __ape_print( const char _FunctionName[], \
                                const char _FileName[], \
                                const unsigned int _LineNumber, \
                                const char* Sentence, ... )
{
	va_list argptr;

	APE_CONFIG_PRINT( "%s: File:%s Line:%u ", _FunctionName, _FileName, _LineNumber );

	va_start( argptr, Sentence );
	APE_CONFIG_VA_PRINT( Sentence, argptr );
	va_end( argptr );

	APE_CONFIG_PRINT( "\n" );
	return;
}
#define ape_print( ... ) __ape_print( __func__, __FILE__, __LINE__, __VA_ARGS__ );

#endif //if defined( __cplusplus )
#else //ifndef DEBUG
	#define ape_print( ... )
#endif //ifndef DEBUG

#endif
