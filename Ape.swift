//
//  Ape.swift
//  LyricLive
//
//  Created by Apple on 2/15/24.
//

import Foundation

public func ape_print(_ message: Any, file: String = #file, function: String = #function, line: Int = #line ) {
#if DEBUG
	print("\(function): File:\(file) Line:\(line) \(message)")
#endif
}
